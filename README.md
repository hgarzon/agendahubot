# Hubot para gestionar y obtener información de las charlas de un evento
Resulta útil sobre todo cuando el evento tiene mas de 1 track y es fácil perderse entre las charlas que están disponibles

# Funcionalidades disponibles
- Obtener el listado de las charlas que tendrán lugar a continuación para cada track
- Obtener el listado de las charlas que tendrán lugar a una hora determinada
- Obtener el listado de las charlas que tendrán lugar en un track determinado
- Indicar que vas a ir a una charla. Sirve además de marca "favorito" ya que se puede obtener un listado de las charlas marcadas
- Obtener listado de los usuarios que van a ir a una charla. 
- Para cada charla se obtiene la siguiente información:
	- Título de la charla
	- Nombre del ponente
	- Hora de inicio y fin
	- Sala donde va a tener lugar

# NOTAS
- En este repositorio solo se almacena el script. El resto de ficheros necesarios para disponer del hubot están desplegados y gestionados en heroku
- Los datos de las charlas se deben modificar para cada evento. Pueden ser añadidos, modificados o eliminados desde los comandos de administración que proporciona el propio hubot