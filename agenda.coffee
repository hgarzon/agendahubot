# Description:
#   Events hubot 
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot get next talks: returns today's next talks
#   hubot get all talks: returns all talks available (past or future)
#   hubot get all future talks: returns all future talks available
#   hubot get talk <id>: returns that talk's information
#   hubot who is going to talk <id>: returns who is going to that talk
#   hubot get my talks: returns information about talks you have said are going to
#   hubot going to talk <id>: say you are going to that talk
#   hubot not going to talk <id>: say you are not going to that talk
#   hubot remove talk <id>: delete that talk
#   hubot add talk name:<talk name>|speaker:<speaker name>|room:<room name>|date:<date>|inittime:<inittime>|endtime:<endtime>: add a talk with that information. <date>=yyyymmdd, <xxxtime>=hhmm
#   hubot edit talk <id> with name:<talk name>|speaker:<speaker name>|room:<room name>|date:<date>|inittime:<inittime>|endtime:<endtime>: edit a talk's information (redefining it). <date>=yyyymmdd, <xxxtime>=hhmm (value ="-" means no modification applied)
#   hubot get times: get talk times for today
#   hubot get tracks: get all tracks
#   hubot get talks for time <time>: get all talks starting at that time (can get times with "get times" command)
#   hubot get talks for track <room name>: get all talks for that room
#   hubot get official web: get the official web url
#   hubot set official web <url>: set the official web url
#
# Notes:
#
# Author:
#	Hector Garzon



module.exports = (robot) ->
	flag_already_responded=0
	cronJob=null
	minutesBeforeTalkStarting=10
	EMOJI_TALK_GOING=":point_up:"

	robot.respond /add all talks$/i, (res) ->
		addTalksFirstDay "20151127"
		addTalksSecondDay "20151128"
		res.send "Charlas añadidas correctamente!"

	robot.respond /add all talks (.+)/i, (res) ->
		day=res.match[1]
		intDayValue=parseInt(day)
		addTalksFirstDay day
		addTalksSecondDay String(intDayValue+1)
		res.send "Charlas añadidas correctamente!"

	robot.respond /reset talks$/i, (res) ->
		flag_already_responded=1
		robot.brain.remove 'talks'
		robot.brain.remove 'maxTalkId',
		res.send "reseteado"

	robot.respond /reset .*/i, (res) ->
		res.send "quizas querias decir 'reset talks'?" if flag_already_responded==0
		flag_already_responded=0

	robot.respond /add talk name:(.+)\|speaker:(.+)\|room:(.+)\|date:(.+)\|inittime:(.+)\|endtime:(.+)/i, (res) ->
		flag_already_responded=1
		addTalk res.match[1], res.match[2], res.match[3], res.match[4], res.match[5], res.match[6]
		res.send "charla añadida"

	robot.respond /add talk .+$/i, (res) ->
		res.send "quizas querias decir 'add talk name:xx|speaker:xx|room:xx|date:xx|inittime:xx|endtime:xx'?" if flag_already_responded is 0
		flag_already_responded=0

	robot.respond /add talk$/i, (res) ->
		res.send "quizas querias decir 'add talk name:xx|speaker:xx|room:xx|date:xx|inittime:xx|endtime:xx'?" if flag_already_responded is 0
		flag_already_responded=0

	# Prueba conceptual de parámetros opcionales
	#robot.respond  /edit talk (.*) with ((date:)([1-9]+)\|)?((name:)(.*)\|)?/i, (res) ->

	# Si ponemos "-" como valor entonces no se modifica ese valor
	robot.respond /edit talk ([0-9]+) with name:(.+)\|speaker:(.+)\|room:(.+)\|date:(.+)\|inittime:(.+)\|endtime:(.+)/i, (res) ->
		flag_already_responded=1
		talks=getTalks()
		talkId=res.match[1]
		talks[talkId]['name']=res.match[2] if res.match[2]!="-"
		talks[talkId]['speaker']=res.match[3] if res.match[3]!="-"
		talks[talkId]['room']=res.match[4] if res.match[4]!="-"
		talks[talkId]['date']=res.match[5] if res.match[5]!="-"
		talks[talkId]['inittime']=res.match[6] if res.match[6]!="-"
		talks[talkId]['endtime']=res.match[7] if res.match[7]!="-"
		robot.brain.set('talks',JSON.stringify(talks))
		#console.log talk for talkId,talk of talks
		#console.log talks[talkId] for talkId in Object.keys(talks)
		res.send "charla modificada correctamente"

	robot.respond /going to talk (.+)/i, (res) ->
		talks=getTalks()
		talkId=res.match[1]
		unless talks[talkId]['going']
			talks[talkId]['going']=[]
		talks[talkId]['going'].push res.message.user.name
		robot.brain.set('talks',JSON.stringify(talks))
		res.send "anotado que vas a la charla: `#{talks[talkId]['name']}`"

	robot.respond /not going to talk (.+)/i, (res) ->
		talks=getTalks()
		talkId=res.match[1]
		unless talks[talkId]['going'] or res.message.user.name not in talks[talkId]['going']
			res.send "no ibas a esa charla.."
			return

		indexItem = talks[talkId]['going'].indexOf(res.message.user.name);
		talks[talkId]['going'].splice(indexItem, 1) if indexItem>-1
		robot.brain.set('talks',JSON.stringify(talks))
		res.send "anotado que no vas a la charla: `#{talks[talkId]['name']}`"

	robot.respond /get my talks/i, (res) ->
		talks=getTalks()
		talksMeGoing=[]
		noTalks=true
		if talks
			for talkId,talk of talks
				if talk['going']?.length>0
					noTalks=false
					res.send getTalkContentText(talk, talkId, res) if res.message.user.name in talk['going']
		if noTalks
			res.send "no vas o has ido a ninguna charla"

	robot.respond /who is going to talk ([0-9]+)/i, (res) ->
		talks=getTalks()
		talkId=res.match[1]
		countPeople=talks[talkId]['going']?.length
		unless talks[talkId]['going'] or countPeople is 0
			res.send "nadie va a esa charla.."
			return
		people=talks[talkId]['going'].reverse().reduce (item1,item2)-> "`#{item1}` , `#{item2}`"
		res.send "van a ir (#{countPeople}): #{people}"

	robot.respond /get talk ([0-9]+)/i, (res) ->
		talkId=res.match[1]
		talks=getTalks()
		unless talks[talkId]?
			res.send "no existe esa charla"
			return
		talkContent=getTalkContentText(talks[talkId], talkId, res)
		res.send talkContent

	robot.respond /remove talk ([0-9]+)/i, (res) ->
		talkId=res.match[1]
		talks=getTalks()
		unless talks[talkId]?
			res.send "no existe esa charla"
			return
		delete talks[talkId]
		robot.brain.set('talks',JSON.stringify(talks))
		res.send "charla eliminada"

	robot.respond /get times$/i, (res) ->
		talks=getTalks()
		times=[]
		if talks
			for talkId,talk of talks
				times.push talk['inittime']if talk['inittime'] not in times
			times.sort()
			timesFormatted=("#{time[0..1]}:#{time[2..3]}"for time in times)
			allTimesText=""
			for time,index in timesFormatted
				allTimesText+="#{time}\n"
			res.send allTimesText
		else
			res.send "no hay charlas"

	robot.respond /get tracks$/i, (res) ->
		talks=getTalks()
		tracks=[]
		if talks
			for talkId,talk of talks
				tracks.push talk['room']if talk['room'] not in tracks
			tracks.sort()
			allTracksText=""
			for track,index in tracks
				allTracksText+="#{track}\n"
			res.send allTracksText
		else
			res.send "no hay charlas"

	robot.respond /get talks for time (.+)/i, (res) ->
		talks=getTalks()
		talksForTime=[]
		time=res.match[1]
		currentDay=getDate()
		talksText=""
		if talks
			for talkId,talk of talks
				talk['talkId']=talkId
				talksForTime.push talk if talk['inittime'] is "#{time[0..1]}#{time[3..4]}" and talk['day']=currentDay
			if talksForTime.length is 0
				res.send "no hay charlas a esa hora" 
				return
			for talk in talksForTime
				talkId=talk['talkId']
				talksText+= getTalkContentText(talk, talkId, res)
				talksText+="\n"
			res.send talksText
		else
			res.send "no hay charlas"

	robot.respond /get talks for track (.+)/i, (res) ->
		talks=getTalks()
		talksForTrack=[]
		track=res.match[1]
		currentDay=getDate()
		talksText=""
		if talks
			for talkId,talk of talks
				talk['talkId']=talkId
				talksForTrack.push talk if talk['room'] is track and talk['day']=currentDay
			if talksForTrack.length is 0
				res.send "no hay charlas para ese track" 
				return
			for talk in talksForTrack
				talkId=talk['talkId']
				talksText+= getTalkContentText(talk, talkId, res)
				talksText+="\n"
			res.send talksText
		else
			res.send "no hay charlas"
			

	robot.respond /get official web$/i, (res) ->
		web=robot.brain.get('officialWebUrl')
		unless web
			res.send "no se ha introducido la web oficial aun"
			return
		res.send web

	robot.respond /set official web (.*)/i, (res) ->
		web=res.match[1]
		robot.brain.set('officialWebUrl',web)
		res.send "grabada la web oficial"

	robot.respond /get official agenda$/i, (res) ->
		agenda=robot.brain.get('officialAgendaUrl')
		unless agenda
			res.send "no se ha introducido la agenda oficial aun"
			return
		res.send agenda

	robot.respond /set official agenda (.*)/i, (res) ->
		agenda=res.match[1]
		robot.brain.set('officialAgendaUrl',agenda)
		res.send "grabada la agenda oficial"

	robot.respond /get all talks$/i, (res) ->
		getInformationAlltalks(res)

	robot.respond /get talks$/i, (res) ->
		getInformationAlltalks(res)

	robot.respond /get all future talks/i, (res) ->
		getInformationAlltalks(res,true)

	robot.respond /get next talks/i, (res) ->
		nextTalks=findNextTalks()
		#console.log nextTalks
		res.send "No hay mas charlas hoy" if nextTalks.length==0
		talks=getTalks()
		res.send getTalkContentText(talks[talkId], talkId, res) for talkId in nextTalks


	# Primer intento de un cronjob real para la agenda
	robot.respond /start cron talks/i, (res)->
		if do createNextCronJob
			res.send "El job se ha creado correctamente"
		else
			res.send "El job no se ha podido crear, puede que sea porque no hay mas charlas hoy"

	# Solo pruebas
	robot.respond /start cron$/i, (res)->
		if cronJob
			cronJob.cronjob.start(robot)
		else
			HubotCron = require 'hubot-cronjob'
			pattern = '*/10 * * * * *'
			timezone = 'Europe/Madrid'
			fn = ->
				robot.messageRoom 'general', 'hola'+getTime()
			cronJob=new HubotCron pattern, timezone, fn

	# Solo pruebas. Cambiamos la programación del job asignándole una nueva instancia de job
	robot.respond /change cron 1$/i, (res)->
		HubotCron = require 'hubot-cronjob'
		pattern = '*/1 * * * * *'
		timezone = 'Europe/Madrid'
		fn = ->
			robot.messageRoom 'general', 'hola: '+getTime()
		cronJob.stop()
		cronJob=new HubotCron pattern, timezone, fn

	# Intento de cambiar la programación sin tener que crear un job nuevo, pero no me funciona por ahora...
	robot.respond /change cron 5$/i, (res)->
		cronJob.stop()
		cronJob.cronjob.cronTime='*/5 * * * * *'
		cronJob.cronjob.start()

	robot.respond /stop cron/i, (res)->
		cronJob.stop()

	#PRIVATE FUNCTIONS

	addTalksFirstDay=(day)->
		console.log "metemos las charlas del día #{day}"
		addTalk "Todo lo que siempre quisiste saber sobre bases de datos distribuídas de alta disponibilidad", "Javier Ramirez", "track 1", day, "1000", "1045"
		addTalk "El arte de ser vago: Clean Code", "Camilo Galiana", "track 2", day, "1000", "1045"
		addTalk "Hardware en el día a dia", "Mario Ezquerro", "track 3", day, "1000", "1045"
		addTalk "Pensando en Big Data: Un paseo por el Modern Data Warehouse", "Ruben Pertusa Lopez, Miguel Egea", "track 4", day, "1000", "1045"
		addTalk "Azure Web Apps - Deep Dive", "Roberto Gonzalez", "track 5", day, "1000", "1045"
		addTalk "El diseño de interfaces a través de los tiempos", "Elena Torro", "track 6", day, "1000", "1045"
		addTalk "Arquitecturas distribuidas en la nube", "Manu Delgado", "track 7", day, "1000", "1045"
		addTalk "Me gusta que los estándares salgan bien.", "Isabel Cabezas", "track 8", day, "1000", "1045"
		addTalk "Scrum Lego. ¡A divertirse ágilmente!", "Silvano Gil Pérez", "track A", day, "1000", "1145"
		addTalk "Transiciones y animaciones en CSS: que empiece el baile", "Luis Calvo Díaz", "track B", day, "1000", "1145"
		addTalk "Taller exprés de Planificación Ágil", "Jose Manuel Beas", "track C", day, "1000", "1145"
		addTalk "Elimina la corrupción: programación funcional pura con Scala", "Juan Manuel Serrano Hidalgo", "track D", day, "1000", "1145"
		addTalk "Beauty Treatment for your Android Application", "Jose L Ugia", "track 1", day, "1100", "1145"
		addTalk "Seguridad en Cloud Pública, de los mitos a la realidad", "OLOF SANDSTROM", "track 2", day, "1100", "1145"
		addTalk "Swift 2 para arquitecturas avanzadas", "Jorge D Ortiz Fuentes", "track 3", day, "1100", "1145"
		addTalk "Cómo Diseñar Software de Calidad en 4 Pasos", "Jose Álvarez de Perea", "track 4", day, "1100", "1145"
		addTalk "Spock: O por qué deberías utilizarlo para testear tu código Java", "Iván López", "track 5", day, "1100", "1145"
		addTalk "Cómo generar una arquitectura cloud autoescalable para tu software LAMP", "Toni Tebas, David Regordosa Avellana", "track 6", day, "1100", "1145"
		addTalk "Web Components + BabylonJS + WebGL, desarrollando mundos 3D con Polymer, ( The Force Awakens  )", "ismael faro", "track 7", day, "1100", "1145"
		addTalk "Deuda Técnica para desarrolladores... ¡y managers!", "Luis García Castro", "track 8", day, "1100", "1145"
		addTalk "Dando amor a los tests", "Joaquin Engelmo Moriche", "track 1", day, "1245", "1330"
		addTalk "Toca Jazz de forma ágil en la cloud", "Jose Miguel Ordax", "track 2", day, "1245", "1330"
		addTalk "Android Wear vs Apple Watch. El futuro en tu muñeca", "Ruth Paramio", "track 3", day, "1245", "1330"
		addTalk "User Driven Development", "David González", "track 4", day, "1245", "1330"
		addTalk "Modern Branding en SharePoint ", "Santiago Porras", "track 5", day, "1245", "1330"
		addTalk "Cross Platform Apps Made Easy with Apache Cordova", "Ben Rondeau", "track 6", day, "1245", "1330"
		addTalk "Recorrido por los \"internals\" de la JVM", "Miguel Angel Pastor Olivar", "track 7", day, "1245", "1330"
		addTalk "Distribute, reactive and scalable platform for data management based on the Lambda architecture", "Daniel Ortiz Esquivel", "track 8", day, "1245", "1330"
		addTalk "Formación Angular JS.", "Carlos Garcia Garcia", "track A", day, "1245", "1430"
		addTalk "Taller de juegos con Swift y SpriteKit para iOS ", "Luis Ramón Álvarez", "track B", day, "1245", "1430"
		addTalk "Geolocalización, Internet of Things y móviles. ", "gnietof", "track C", day, "1245", "1430"
		addTalk "Mapas inteligentes con AngularJS y ArcGIS", "Raúl Jiménez Ortega", "track D", day, "1245", "1430"
		addTalk "Machine Learning para todos con Azure ML y el proyecto Oxford", "Juan Manuel Servera", "track 1", day, "1345", "1430"
		addTalk "Como hacer tuning a capas de acceso a datos en .NET", "Enrique Catala", "track 2", day, "1345", "1430"
		addTalk "Automatiza tu flow en iOS", "Jorge Maroto García", "track 3", day, "1345", "1430"
		addTalk "Haciendo #noProject en Grandes Organizaciones", "Unai Roldán", "track 4", day, "1345", "1430"
		addTalk "Internet de las cosas industrial aplicado a Eficiencia Energética", "David Fernández González, Carlos Javier Prados Hijón", "track 5", day, "1345", "1430"
		addTalk "Material Animations: show me the code!", "Luis González", "track 6", day, "1345", "1430"
		addTalk "RxJava: qué es y por qué mola", "Sergio Antonio Delgado Quero", "track 7", day, "1345", "1430"
		addTalk "Organiza tu front con Dart y Polymer", "Pablo Gonzalez Doval", "track 8", day, "1345", "1430"
		addTalk "Stop making fool of yourself about documentation!", "Guillaume Scheibel", "track 1", day, "1600", "1645"
		addTalk "Akka.Net: El modelo de programación con actores", "Javier García Magna", "track 2", day, "1600", "1645"
		addTalk "Design & be reactive on Android with Iron Man in a clean way", "Saul Molinero Malvido", "track 3", day, "1600", "1645"
		addTalk "CartoDB: mapas dinámicos, rápidos y eficientes con PostgreSQL", "Raúl Ochoa", "track 4", day, "1600", "1645"
		addTalk "Machine Learning en tu web con AzureML", "Ruben Pertusa Lopez, Miguel Egea", "track 5", day, "1600", "1645"
		addTalk "DevOps Mashup. Microservicios, ELK, Cloud y Entrega Continua.", "Ramón Román Nissen, Ignacio Sánchez Ginés", "track 6", day, "1600", "1645"
		addTalk "Aspecto útiles de Machine Learning", "Santiago Porras, Adrian Diaz Cervera", "track 7", day, "1600", "1645"
		addTalk "La arquitectura fluida", "Alex Fernandez", "track 8", day, "1600", "1645"
		addTalk "Internet of your things with Raspberry Pi 2 and the cloud", "Juan Manuel Servera", "track A", day, "1600", "1745"
		addTalk "Cambiar una empresa con juegos agiles", "Javier Gamarra, Soraya Vay Burgoa", "track B", day, "1600", "1745"
		addTalk "Spark hands-on", "Jorge Lopez-Malla, Gaspar Muñoz", "track C", day, "1600", "1745"
		addTalk "Iniciación a React y ES6 con webpack", "Ricardo Borillo", "track D", day, "1600", "1745"
		addTalk "Postgres como base de datos NoSql", "Ruben Gomez Garcia", "track 1", day, "1700", "1745"
		addTalk "Primeros pasos con Go", "Daniel González Cerviño", "track 2", day, "1700", "1745"
		addTalk "Microservices and testing... talking from the experience", "Jairo González Pérez", "track 3", day, "1700", "1745"
		addTalk "La herencia es la clase base de todos los problemas", "JOSE DANIEL GARCIA SANCHEZ", "track 4", day, "1700", "1745"
		addTalk "Programación con Silex. Las cosas SI se pueden hacer bien con PHP", "Daniel Primo", "track 5", day, "1700", "1745"
		addTalk "Desarrollo móvil con Xamarin y F#", "Alex Casquete", "track 6", day, "1700", "1745"
		addTalk "Akka voló sobre el nido del Future", "Javier Santos Paniego, David Vallejo Navarro", "track 7", day, "17000", "1745"
		addTalk "Clean Architecture for Android", "Jose Manuel Pereira Garcia", "track 8", day, "1700", "1745"
		addTalk "Blues", "Chema Alonso", "track 1", day, "1800", "1845"
		addTalk "Kotlin for Android Developers", "Antonio Leiva Gordillo", "track 2", day, "1800", "1845"
		addTalk "He fracasado : Tengo mas de 30 y sigo programando", "Katia Aresti", "track 3", day, "1800", "1845"
		addTalk "Patrones de diseño con Java 8", "Alonso Javier Torres Ortiz", "track 4", day, "1800", "1845"
		addTalk "Construyendo tu propia bola de cristal: predicción de precios con machine learning", "Rafael Bermúdez Míguez", "track 5", day, "1800", "1845"
		addTalk "React Native - El poder de React en el desarrollo móvil", "Eduard Tomàs", "track 6", day, "1800", "1845"
		addTalk "La complejidad oculta de importar CSVs geoespaciales", "Diego 'Kartones' Muñoz Perez", "track 7", day, "1800", "1845"
		addTalk "At your service! Practical uses of Service Workers", "Salvador de la Puente González", "track 8", day, "1800", "1845"
		addTalk "Procesamiento en Tiempo Real con Meteor", "David Collado, Jorge Barrachina, Francisco Calle Moreno, Luis Herranz", "track A", day, "1800", "1930"
		addTalk "Recetas para visualización de datos en la web con CartoDB", "Jorge Arévalo", "track B", day, "1800", "1930"
		addTalk "Arquitectura y testing, el mantra del desarrollador Android feliz", "Pablo Guardiola, Joaquin Engelmo Moriche", "track C", day, "1800", "1930"
		addTalk "Primeros pasos con Akka: Olvídate de los threads", "Juan Jose Lopez Martin", "track D", day, "1800", "1930"

	addTalksSecondDay=(day)->
		addTalk "Como mantener tu código PHP más \"limpio\" ", "Oscar Vítores", "track 1", day, "0930", "1015"
		addTalk "La persistencia tiene un límite", "Emma Sesmero, Blanca Hernández", "track 2", day, "0930", "1015"
		addTalk "Functional Reactive Programming: FP, Javascript con extra de bacon.", "Javier Onielfa", "track 3", day, "0930", "1015"
		addTalk "Tendencias del sector IT en España: Sueldos, tecnologías y empleo", "Codemotion Madrid", "track 3", day, "0930", "1015"
		addTalk "Del infierno al cielo", "Raul Requero", "track 5", day, "0930", "1015"
		addTalk "Rust, el lenguaje que reemplazará a C y C++", "Roberto Perez", "track 6", day, "0930", "1015"
		addTalk "Unity3D + Kinect + Oculus + Leap = BoomShakalaka!!!", "Toni Recio Sacristà", "track 7", day, "0930", "1015"
		addTalk "Gestión masiva de datos en la era IoT", "Luis Guerrero", "track 8", day, "0930", "1015"
		addTalk "Android to wear", "Daniel Rojo Pérez", "track A", day, "0930", "1115"
		addTalk "Caminando de Java a Scala en menos de 2 horas", "Sergio Gómez, Abel Rincón Matarranz", "track B", day, "0930", "1115"
		addTalk "Desarrollar un videojuego móvil multiplataforma con Cocos2D-X", "Jon Segador", "track C", day, "0930", "1115"
		addTalk "Spock: testing (in the) Enterprise", "Fernando Redondo", "track D", day, "0930", "1115"
		addTalk "Cassandra para impacientes", "Carlos Alonso Pérez", "track 1", day, "1030", "1115"
		addTalk "World-Class Testing Pipeline in Android", "Pedro Vicente Gómez Sánchez", "track 2", day, "1030", "1115"
		addTalk "De Java a Python en un gestor de dependencias de C y C++: Aventuras y desventuras de una startup", "Diego Rodriguez-Losada", "track 3", day, "1030", "1115"
		addTalk "Hackathons on Rails", "Codemotion", "track 4", day, "1030", "1115"
		addTalk "Coding Culture", "Sven Peters", "track 5", day, "1030", "1115"
		addTalk "Unit testing: el mito de los cero bugs", "Fernando Escolar", "track 7", day, "1030", "1115"
		addTalk "Bienvenido de nuevo C++", "Eduard Tomàs", "track 8", day, "1030", "1115"
		addTalk "Web components, como implementarlos en nuestros desarrollos.", "Brian Salazar", "track 1", day, "1215", "1300"
		addTalk "Rendimiento invisible: ¿Cómo de rápido es tú código JavaScript?", "Kiko Beats", "track 2", day, "1215", "1300"
		addTalk "Dos viajes dentro de la JVM: CRaSH y YouDebug", "jmiguel rodriguez", "track 3", day, "1215", "1300"
		addTalk "Universal Windows Platform: Como planificar y diseñar mi Aplicación", "Martin Vega Caballero", "track 4", day, "1215", "1300"
		addTalk "Programando en diferido", "Jorge J. “flipper83” Barroso", "track 5", day, "1215", "1300"
		addTalk "Comunicación en equipos técnicos, o cómo tratar con tus colegas sin que te den ganas de matar", "javier ramirez", "track 6", day, "1215", "1300"
		addTalk "Scrum wars: destripando Scrum", "David Fernandez", "track 7", day, "1215", "1300"
		addTalk "DevOps4Networks", "Nathan Sowatskey", "track 8", day, "1215", "1300"
		addTalk "Ibm containers-solving problems at light speed with a hosted Docker service", "Rick Osowski", "track A", day, "1215", "1400"
		addTalk "Graphite a fondo.", "Jose Plana", "track C", day, "1215", "1400"
		addTalk "RxJava en práctica, de 0 a master", "Javier Gamarra", "track D", day, "1215", "1400"
		addTalk "Carrera de fondo: la continuada lucha de AngularJS... ¿Quién gana?", "José Manuel García García", "track 1", day, "1315", "1400"
		addTalk "Top 10 vulnerabilidades cometidas por desarroladores y administradores", "Jaime Odena", "track 2", day, "1315", "1400"
		addTalk "La Web Orientada a Componentes", "Javier Vélez Reyes", "track 3", day, "1315", "1400"
		addTalk "Machine Learning y el Proyecto Oxford", "Alejandro Campos Magencio", "track 4", day, "1315", "1400"
		addTalk "Navega con el Piloto Automático con CasperJS", "Adolfo Sanz De Diego", "track 5", day, "1315", "1400"
		addTalk "Realtime video streaming the opensource way", "Ivan Belmonte", "track 6", day, "1315", "1400"
		addTalk "¿Quién manda en tu lenguaje de programación favorito?", "Luis García Castro", "track 7", day, "1315", "1400"
		addTalk "Build Apps for Apple Watch", "Francesco Novelli", "track 8", day, "1315", "1400"
		addTalk "Todo lo que siempre quisiste saber sobre Atlassian y nunca te atrevista a preguntar", "Antonio David Fernandez", "track B", day, "1315", "1400"
		addTalk "Tu DevOp me da trabajo: Soy auditor de seguridad", "Daniel Garcia (a.k.a. cr0hn)", "track 1", day, "1530", "1615"
		addTalk "Descubriendo 40 Geo-APIs en 40 minutos", "Raúl Jiménez Ortega", "track 2", day, "1530", "1615"
		addTalk "Adding Realtime to your App", "Nacho Martín", "track 3", day, "1530", "1615"
		addTalk "Containers-the little linux feature revolutionizing apps of all sizes", "Rick Osowski", "track 4", day, "1530", "1615"
		addTalk "Proyecto TRRP: Drones con routers, Javascript y C", "David Melendez", "track 5", day, "1530", "1615"
		addTalk "Drilling into Data with Apache Drill", "Tugdual Grall", "track 6", day, "1530", "1615"
		addTalk "ASP.NET 5 the new open-source and cross-platform framework", "Marc Rubiño", "track 7", day, "1530", "1615"
		addTalk "Be water with Spark", "Sergio Gómez", "track 8", day, "1530", "1615"
		addTalk "Testing AngularJS with Jasmine", "Sam Leach", "track A", day, "1530", "1715"
		addTalk "Scraping the web", "Jose Manuel Ortega", "track B", day, "1530", "1715"
		addTalk "Building and deploying a distributed application with Docker, Mesos and Marathon", "Julia Mateo", "track C", day, "1530", "1715"
		addTalk "Elimina la corrupción: programación funcional pura con Scala", "Juan Manuel Serrano Hidalgo", "track D", day, "1530", "1715"
		addTalk "Resolviendo la noche electoral con AWS + Node.js + Angular.js + D3.js + Leafleat.js", "Javier Abadía", "track 1", day, "1630", "1715"
		addTalk "Help! I need more women!", "Laura Morillo-Velarde Rodríguez, Amaia Castro, ana fernandezdevega, Marina Lorenzo", "track 2", day, "1630", "1715"
		addTalk "¡EMT Madrid y Apple Watch! Aplicaciones Dependientes de Contexto Siguiente Nivel", "Alejandro Zaragoza, Jorge Ruiz", "track 3", day, "1630", "1715"
		addTalk "Testing Your Spring Boot Application With Selenium ", "Mathilde Rigabert Lemée", "track 4", day, "1630", "1715"
		addTalk "Windows 10: One package for all", "Josue Yeray Julian Ferreiro", "track 5", day, "1630", "1715"
		addTalk "CoreOS: Tu infrastructura escalable y reproducible", "Luis Martínez de Bartolomé", "track 6", day, "1630", "1715"
		addTalk "Realidad Virtual de andar por casa | Shirt-sleeve Virtual Reality", "William Viana, Jorge Rodríguez Lería", "track 7", day, "1630", "1715"
		addTalk "Primeros pasos con Aurelia", "Raul Requero, Jose Angel", "track 8", day, "1630", "1715"
		addTalk "MongoDB Avanzado", "Víctor Cuervo", "track 1", day, "1730", "1815"
		addTalk "Taiga: de 0 a 70000 proyectos online. Como para los pies a tus usarios", "Pablo Ruiz Muzquiz", "track 2", day, "1730", "1815"
		addTalk "Análisis continuo de la calidad del software en desarrollos ágiles", "Guillermo Villarrubia Esteban", "track 3", day, "1730", "1815"
		addTalk "The HTTP/2 protocol: kill that latency!", "Giuseppe Ciotta", "track 4", day, "1730", "1815"
		addTalk "Programa mejor que Chuck Norris: Herramientas que nos ayudan a mejorar", "Rafael Bermúdez Míguez", "track 5", day, "1730", "1815"
		addTalk "Elegant?? Unit Testing", "Pablo Guardiola", "track 6", day, "1730", "1815"
		addTalk "Scale up your CI to a Continuous Delivery Pipeline", "Miguel Serrano Milano", "track 7", day, "1730", "1815"
		addTalk "Ansible: gozando la configuración de servidores", "Ignacio Sánchez Holgueras", "track 8", day, "1730", "1815"
		addTalk "Creación de un preprocesador simple de HTML con NodeJS", "Brian Salazar", "track A", day, "1730", "1915"
		addTalk "Java 8 en accion", "Denis Simon Soneira, Aurita Muñoz, Katia Aresti", "track B", day, "1730", "1915"
		addTalk "Build a times Series Application with with Kafka, HBase and Spark", "Tugdual Grall", "track C", day, "1730", "1915"
		addTalk "Creando aplicaciones de iOS con React Native", "Sam Leach", "track D", day, "1730", "1915"
		addTalk "Mesa redonda: la nueva generación de starups españolas", "Nacho coloma, David Bonilla, Felipe Navio, Javier Santana", "track 1", day, "1830", "1915"
		addTalk "Escalando Agile en las organizaciones", "Chema Garcia Martinez", "track 2", day, "1830", "1915"
		addTalk "«Gente que hace cosas con cacharros»", "Miguel Ángel López Vicente, José Juan Sánchez Hernández", "track 3", day, "1830", "1915"
		addTalk "Integración continua con Apps Xamarin", "Javier Suárez Ruiz", "track 4", day, "1830", "1915"
		addTalk "Como arreglar este desastre", "Samuel Solís Fuentes", "track 5", day, "1830", "1915"
		addTalk "Radar Ágil: herramienta de crecimiento para equipos ágiles", "Juan Manuel Gómez Ramos", "track 6", day, "1830", "1915"
		addTalk "PostCSS y CSSnext: El CSS del futuro.", "Xaviju Julián", "track 7", day, "1830", "1915"
		addTalk "Testing Android Security", "Jose Manuel Ortega", "track 8", day, "1830", "1915"

	addTalk=(name, speaker, room, date, inittime, endtime)->
		maxTalkId=robot.brain.get('maxTalkId')
		unless maxTalkId
			maxTalkId=0
		maxTalkId++
		robot.brain.set('maxTalkId',maxTalkId)

		talks=getTalks()
		unless talks
			talks={}
		unless talks[maxTalkId]
			talks[maxTalkId]={}
		talks[maxTalkId]['name']=name
		talks[maxTalkId]['speaker']=speaker
		talks[maxTalkId]['room']=room
		talks[maxTalkId]['date']=date
		talks[maxTalkId]['inittime']=inittime
		talks[maxTalkId]['endtime']=endtime
		robot.brain.set('talks',JSON.stringify(talks))

	createNextCronJob=->
		nextTalkTime=getNextTalkTime()
		#currentTimeAdjusted=addTime(getTime(),minutesBeforeTalkStarting)
		#if not nextTalkTime>currentTimeAdjusted
		#	console.log "No da tiempo a programarlo de nuevo"
		#	return false
		pattern=convertTimeToCronPatterTime(nextTalkTime) if nextTalkTime>0
		if pattern?
			console.log "hay pattern asi que creare el job para la hora #{pattern}"
			HubotCron = require 'hubot-cronjob'
			timezone = 'Europe/Madrid'
			fn = ->
				nextTalks=findNextTalks(false)
				talks=getTalks()
				#robot.messageRoom 'general', "Las siguientes charlas empiezan en #{minutesBeforeTalkStarting} minutos:"
				txt="Las siguientes charlas empiezan en breve:\n"
				for talkId in nextTalks
					# Comprobamos que realmente empieza en esos minutos y que no es simplemente la siguiente charla del track
					talk=talks[talkId]
					if addTime(getTime(),minutesBeforeTalkStarting+1)>talk["inittime"]
						txt+=getTalkContentText(talks[talkId], talkId)
						txt+="\n"
				robot.messageRoom 'general', txt
				createNextCronJob()
			cronJob=new HubotCron pattern, timezone, fn
			return true
		else
			console.log "No se ha podido asignar un nuevo job"
			return false

	convertTimeToCronPatterTime= (time)->
		if time?> 0
			# Quitamos unos minutos al valor del time para avisar antes de que empiece la charla
			timeAdjusted = time - minutesBeforeTalkStarting
			timeAdjustedText=String(timeAdjusted)
			timeAdjusted -= 40 if timeAdjustedText[2..3]>59
			timeAdjustedText=String(timeAdjusted)
			timeAdjustedText="0#{timeAdjustedText}" if timeAdjustedText.length<4
			return "0 #{timeAdjustedText[2..3]} #{timeAdjustedText[0..1]} * * *"
		else
			return null


	getNextTalkTime= ->
		talks=getTalks()
		nextTalkIds=findNextTalks(true)
		return 0 if nextTalkIds.length is 0
		#Versión 1
		soonerTime=2359
		for talkId in nextTalkIds
			talk=talks[talkId]
			soonerTime=talk.inittime if talk.inittime<soonerTime
		#Versión 2
		#sonnerTime=Math.min (talk.inittime for talk in nextTalks)...
		return soonerTime

	getInformationAlltalks= (res, onlyFutureTalks=false)->
		talks=getTalks()
		currentTime=getTime()
		currentDate=getDate()
		numberOfTalksLimit=1500
		talksText=""
		if talks
			index=1
			for talkId,talk of talks
				if index<numberOfTalksLimit
					#Mandamos todas las charlas en un mismo mensaje para que sea mas rápida la respuesta
					talksText+= getTalkContentText(talk, talkId, res) if not onlyFutureTalks or (talk['inittime']>currentTime and (talk['date']==currentDate or talk[date]>currentDate))
					talksText+="\n"
					index++
				else
					res.send talksText
					res.send "... hay un total de #{(Object.keys(talks)).length} charlas, pero por claridad no muestro todas"
					return
			res.send talksText
		else
			res.send "no hay charlas"


	getTextPeopleBasedOnNumber= (numberOfPeople)->
		if numberOfPeople>1 
			"#{numberOfPeople} personas"
		else if numberOfPeople is 1
			"1 persona"
		else
			"ninguna persona"

	getTalkContentText= (talk, talkId, res, emoji="") ->
		console.log "Entro en getTalkContentText"
		talkName=talk['name']
		talkSpeaker=talk['speaker']
		talkRoom=talk['room']
		talkDate=talk['date']
		talkInitTime=talk['inittime']
		talkEndTime=talk['endtime']
		console.log "campos asignados"
		if talk['going']? and talk['going'].length>0 
			console.log "hay gente que va"
			numberPeopleGoing=talk['going'].length
			#Si no tengo un emoji ya le pongo uno para marcar a las que voy
			if (res?)
				emoji=EMOJI_TALK_GOING if res.message.user.name in talk['going'] and emoji is ""
		else
			console.log "no hay gente que vaya"
			numberPeopleGoing=0
		console.log "antes de asignar textPeopleBasedOnNumber"
		textPeopleBasedOnNumber=getTextPeopleBasedOnNumber(numberPeopleGoing)
		console.log "despues de asignar textPeopleBasedOnNumber"
		talkContent="#{emoji} [#{talkId}] `#{talkName}` por `#{talkSpeaker}` en `#{talkRoom}`, de *_#{talkInitTime}_* a *_#{talkEndTime}_* (#{textPeopleBasedOnNumber})"
		console.log "devuelvo: #{talkContent}"
		return talkContent

	getTime= ->
		date = new Date()
		hour = date.getHours()
		hour = (if hour < 10 then "0" else "") + hour
		min  = date.getMinutes()
		min = (if min < 10 then "0" else "") + min
		return hour + min

	getDate= ->
		date = new Date()
		year = date.getFullYear()
		month = date.getMonth() + 1
		month = (if month < 10 then "0" else "") + month
		day  = date.getDate()
		day = (if day < 10 then "0" else "") + day
		return year + month + day

	addTime= (time,minutesBeforeTalkStarting)->
		timeAdjusted=parseInt(time)+parseInt(minutesBeforeTalkStarting)
		timeAdjustedText=String(timeAdjusted)
		timeAdjustedText="0#{timeAdjustedText}" if timeAdjustedText.length<4
		timeAdjusted += 40 if timeAdjustedText[2..3]>59
		timeAdjusted



	findNextTalks= (timeIsAdjusted=false)->
		talks=getTalks()
		if timeIsAdjusted
			currentTime=addTime(getTime(),minutesBeforeTalkStarting)
		else
			currentTime=getTime()
		currentDate=getDate()
		talksSelected={}

		for talkId, talk of talks
			room=talk.room
			inittime=talk.inittime
			talkdate=talk.date
			if talkdate is currentDate
				if inittime>currentTime
					if not talksSelected[room]?
						talksSelected[room]=talk
						talksSelected[room].id=talkId
					else 
						if talksSelected[room].inittime>inittime
							talksSelected[room]=talk
							talksSelected[room].id=talkId
		###
		for talkId in Object.keys(talks)
			talk=talks[talkId]
			room=talk.room
			inittime=talk.inittime
			talkdate=talk.date
			if talkdate is currentDate
				if inittime>currentTime
					if not talksSelected[room]?
						talksSelected[room]=talk
						talksSelected[room].id=talkId
					else 
						if talksSelected[room].inittime>inittime
							talksSelected[room]=talk
							talksSelected[room].id=talkId
		###
		talksSelectedArray=[]
		if talksSelected
			for room, talk of talksSelected
				talksSelectedArray.push(talk.id)
			#for room in Object.keys(talksSelected)
			#	talk=talksSelected[room]
			#	talksSelectedArray.push(talk.id)
		return talksSelectedArray

	getTalks= ->
		JSON.parse(robot.brain.get('talks'))


	#CRON

	#npm install --save hubot-cronjob
	#HubotCron = require 'hubot-cronjob'
	#pattern = '0 */59 * * * *'
	#timezone = 'Europe/Madrid'
	#fn = ->
	#	robot.messageRoom 'general', '*Próximas charlas:*'
	#	robot.messageRoom 'general', getNextTalksText
	#new HubotCron pattern, timezone, fn


	###
	   INICIO DE PRUEBAS

	getNextTalksText= "1) `Charla x` - _13:50h_ - *Autor de la charla* - http://www.google.com\n2) *Charla Y* - _14:45h_ - `Autor de la charla` - http://www.google.com"

	robot.respond /text me/i, (msg) ->
		msg.send getNextTalksText
		robot.messageRoom "privado-hector", "para el privado.."

	robot.respond /tellme/i, (res) ->
		console.log Object(res)
		res.send "que quieres que te diga muchacho.."


	robot.respond /dime los datos de la charla (.*) y la información de su (.*)/i, (res) ->
		charla = res.match[1]
		informacion = res.match[2]
		res.send "la charla #{charla} es XX y respecto a #{informacion}, no se nada en estos momentos"

	robot.respond /graba esto (.*)/i, (res) ->
		robot.brain.set 'talks', res.match[1]
		res.send "guardado"

	robot.respond /muestrame lo guardado/i, (res) ->
		valor=robot.brain.get('talks')
		res.send "tengo guardado el valor: #{valor}"
		jsonObj = JSON.parse(valor)
		valorCampoAutor=jsonObj['autor']
		res.send "valor del campo autor: #{valorCampoAutor}"
		valorCampoHoraInicio=jsonObj['horaInicio']
		res.send "valor del campo autor: #{valorCampoHoraInicio}"

	robot.respond /get time/i, (res) ->
		res.send getTime()
		#robot.emit 'slack-attachment',
		#	channel: "general"
		#	username: "hecbot"
		#	icon_url: "https://slack.global.ssl.fastly.net/9fa2/img/services/hubot_128.png"
		#	content:
		#		fallback: "fallback"
		#		title: "works now"
		#		title_link: "https://github.com"
		#		text: "Shows up"
		#		image_url: "https://assets-cdn.github.com/images/modules/logos_page/GitHub-Logo.png"
		#		color: "#111111"

	robot.respond /get date/i, (res) ->
		res.send getDate()


	FIN DE PRUEBAS
	###
	